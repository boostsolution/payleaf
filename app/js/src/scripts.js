'use strict';
$(function () {
	var trigger = $('#hamburger'),
		isClosed = true,
		$ww = $(window).width(),
		liElement = $('#nav .navbar-nav >li');
	trigger.click(function() {
		burgerTime();
	});
	//animate botton mobile
	if ($ww < 1028) {
		$('#nav .nav-group').hide('slow');
	}
	function burgerTime() {
		if (isClosed == true) {
			trigger.removeClass('is-closed');
			trigger.addClass('is-open');
			isClosed = false;
			$('#nav .nav-group').addClass('active');
			$('#nav .nav-group').show('slow');
		} else {
			isClosed = true;
			setTimeout(function() {
				trigger.removeClass('is-open');
				trigger.addClass('is-closed');
				$(liElement).removeClass('open');
				$('#nav .nav-group').removeClass('active');
				$('#nav .nav-group').hide('slow');
			}, 400);
		}
	}

	var navResize = function () {
		$ww = $(window).width();
		if ($ww > 1028) {
			$('#nav .nav-group').show();
			$(liElement).removeClass('open');
		} else {
			$('#nav .nav-group').hide();
			isClosed = true;
			setTimeout(function() {
				trigger.removeClass('is-open');
				trigger.addClass('is-closed');

				$('#nav .nav-group').removeClass('active');
			}, 400);
		}
	};

	window.addEventListener('resize', navResize, false);

	$(liElement).click(function () {
		$(liElement).removeClass('open');
		if ($ww < 1028) {
			$(this).addClass('open');
		}
	})
});