var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync = require('browser-sync'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglifyjs'),
	cssnano = require('gulp-cssnano'),
	rename = require('gulp-rename'),
	del = require('del'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	cache = require('gulp-cache'),
	autoprefixer = require('gulp-autoprefixer'),
	spritesmith = require('gulp.spritesmith'),
	sourcemaps = require('gulp-sourcemaps'),
	livereload = require('gulp-livereload');

//sass
gulp.task('sass', function () {
	return gulp.src('app/sass/**/*.+(scss|sass)')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest('app/css'));
});

gulp.task('css-libs', ['sass'], function () {
	return gulp.src('app/css/main.css')
		.pipe(cssnano())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('app/css'));
});

//sprite
gulp.task('sprite', function () {
	var spriteData = gulp.src('app/img/sprites/*.png')
		.pipe(spritesmith({
			imgName: 'sprite-@1x.png',
			cssName: '../sass/_sprite.sass',
			imgPath: "../img/sprite-@1x.png",

			algorithm: 'top-down',
			padding: 20,

			retinaSrcFilter: 'app/img/sprites/*@2x.png',
			retinaImgName: 'sprite-@2x.png',
			retinaImgPath: "../img/sprite-@2x.png"
		}))
		.pipe(browserSync.reload({stream: true}));
	return spriteData.pipe(gulp.dest('app/img/'));
});

//script
gulp.task('scripts', function () {
	//development
	return gulp.src(['app/libs/jquery/dist/jquery.min.js', 'app/js/src/**/*.js'])
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest('app/js'));

	//minify production
	// return gulp.src(['app/libs/jquery/dist/jquery.min.js', 'app/js/src/**/*.js'])
	// 	.pipe(concat('main.min.js'))
	// 	.pipe(uglify())
	// 	.pipe(gulp.dest('app/js'));
});

//server
gulp.task('browser-sync', function () {
	browserSync.init({
		server: {
			baseDir: 'app'
		},
		host: 'localhost',
		port: 9004,
		notify: false
	});
});

//clean
gulp.task('clean', function () {
	return del.sync('dist');
});

gulp.task('clear', function () {
	return cache.clearAll();
});

//img
gulp.task('img', function () {
	return gulp.src('app/img/**/*')
		.pipe(cache(imagemin({
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			une: [pngquant()]
		})))
		.pipe(gulp.dest('dist/img'))
		.pipe(browserSync.reload({stream: true}));
});

//build
gulp.task('build', ['clean', 'img', 'sass', 'scripts'], function () {
	var buildCss = gulp.src([
		'app/css/main.css',
		'app/css/main.min.css',
	])
		.pipe(gulp.dest('dist/css'));
	var buildFonts = gulp.src('app/fonts/**/*')
		.pipe(gulp.dest('dist/fonts'));
	var buildJs = gulp.src('app/js/**/*')
		.pipe(gulp.dest('dist/js'));
	var buildHtml = gulp.src('app/*.+(html|php)')
		.pipe(gulp.dest('dist'));
});

//watch
gulp.task('watch', function () {
	livereload.listen();
	gulp.watch('app/sass/**/*.+(scss|sass)', ['sass', 'css-libs']);
	gulp.watch('app/js/src/**/*.js', ['scripts']);
	gulp.watch('app/css/main.css', function (file) {
		livereload.changed(file)
	});

});

//default development tasks
gulp.task('default', ['sprite', 'sass', 'css-libs', 'scripts', 'browser-sync', 'watch']);